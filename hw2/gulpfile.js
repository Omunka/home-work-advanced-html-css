const gulp = require("gulp");

const clearDistFolder = require("./gulp-tasks/cleanDist").cleanDist;
const htmlTask = require("./gulp-tasks/html").html;
const imgTask = require("./gulp-tasks/img").img;
const cssTask = require("./gulp-tasks/styles").styles;
const jsTask = require("./gulp-tasks/js").js;
const browserSync = require('browser-sync').create();

// function devTask() {
//     return gulp.series(clearDistFolder, gulp.parallel(htmlTask, cssTask, imgTask, jsTask)); 
// }

// function watchFiles() {
//     gulp.watch(['src/'], devTask());
// }


// // exports.dev = devTask();
// exports.watch = watchFiles;



gulp.task("build", gulp.series(clearDistFolder, gulp.parallel(htmlTask, cssTask, jsTask, imgTask)));

gulp.task("dev", () => {
    browserSync.init({
        server: {
            baseDir: "build"
        }
    })
    gulp.watch("src/scss/*.scss").on('change', gulp.series(cssTask, browserSync.reload));
    gulp.watch("src/*.html").on('change', gulp.series(htmlTask, browserSync.reload));
    gulp.watch("src/js/*.js").on('change', gulp.series(jsTask, browserSync.reload));
});

