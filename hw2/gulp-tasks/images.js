const gulp = require("gulp");

function handleImgFiles() {
    return  gulp.src("src/img/**")
    .pipe(gulp.dest('build/img'));
}
exports.img = handleImgFiles;