
const btn = document.querySelector(".header__burger");
const menu = document.querySelector(".header__menu");

btn.addEventListener("click", function(e){
    btn.classList.toggle("active");
    menu.classList.toggle("active");
});
